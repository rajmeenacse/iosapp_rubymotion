class AppDelegate
  def application(application, didFinishLaunchingWithOptions:launchOptions)

    @window = UIWindow.alloc.initWithFrame(UIScreen.mainScreen.bounds)
    @window.makeKeyAndVisible

    # This is our new line!
    controller = TapController.alloc.initWithNibName(nil, bundle: nil)

    nav_controller = UINavigationController.alloc.initWithRootViewController(controller)
    alphabet_controller = AlphabetController.alloc.initWithNibName(nil, bundle: nil)
    testmuse_controller = UIViewController.alloc.initWithNibName(nil, bundle: nil)
    testmuse_controller.title = "TestMuse!"
    testmuse_controller.view.backgroundColor = UIColor.purpleColor

    tab_controller = UITabBarController.alloc.initWithNibName(nil, bundle: nil)
    tab_controller.viewControllers = [alphabet_controller, nav_controller, testmuse_controller]
    @window.rootViewController = tab_controller

    true

  end
end
